import { Fragment, useState, useEffect } from "react";
import FormControl from "./FormControl";
import "./form.css";


export default function UserForm({onSubmit}) {

  const defaultData = {
    firstName: "",
    lastName: "",
    age: "",
    employed: false,
    selectedColor: "",
    sauces: [],
    stooge: "larry",
    note: "",
  };

  
const defaultErrorState = {
    firstName:false,
    lastName:false,
    age:false,
    note:false
  };


  const [data, setData] = useState(defaultData);
  const [error, setError] = useState(defaultErrorState);
  const [isFormChanged, setFormChanged] = useState(false);
  const [isFormValid, setFormValid] = useState(false);
  const [sauces, setSouceList] = useState([]);
  const [stooges, setStoogeList] = useState([]);

  const displayData = JSON.stringify(data, null, 4);

  const isAlphabetic = (value) => /^[A-Za-z\s]+$/.test(value);
  const isNumeric = (value) => /^[0-9]+$/.test(value);

  const handleChange = (e) => {
     let value;

     switch(e.target.name){
        case "employed": value = e.target.checked; break;
        case "sauces":
            if(e.target.checked){
                value = [...data.sauces, e.target.value]
            }else{
                value = data.sauces.filter(item=>item !== e.target.value)
            }
        break;
        default: value = e.target.value
     }

    setData({ ...data, [e.target.name]: value });

    setFormChanged(true);
  };

  const handleSubmit = (e)=>{
    e.preventDefault();
    
    if(!Object.values(error).some(Boolean)){
      onSubmit(data);
    }

  }

  const handleReset = ()=>{
    setData(defaultData);
    setError(defaultErrorState);
    setFormChanged(false);
  }


  useEffect(()=>{
    
   const errors = {};

     if(!isAlphabetic(data.firstName)){
        errors.firstName = true;
     }
 
     if(!isAlphabetic(data.lastName)){
         errors.lastName = true;
      }
 
      if(!isNumeric(data.age)){
         errors.age = true;
      }
 
      if(data.note.length > 100){
         errors.note = true
      }
 
      setError({...error, ...errors});
 
      if(Object.keys(errors).length > 0){
         setFormValid(false);
       }else{
         setFormValid(true);
       }
  }, [data])

  useEffect(()=>{
    fetch('data/sauceList.json')
    .then((response)=>response.json())
    .then((data)=>setSouceList(data));
  }, [])

  useEffect(()=>{
    fetch('data/stoogeList.json')
    .then((response)=>response.json())
    .then((data)=>setStoogeList(data));
  }, [])



  return (
    <>
    <form onSubmit={handleSubmit}>
      <FormControl label="First Name">
        <input
          name="firstName"
          placeholder="First Name"
          value={data.firstName}
          onChange={handleChange}
          className={error.firstName ? 'error' : ''}
        />
      </FormControl>

      <FormControl label="Last Name">
        <input
          name="lastName"
          placeholder="Last Name"
          value={data.lastName}
          onChange={handleChange}
          className={error.lastName ? 'error' : ''}
        />
      </FormControl>

      <FormControl label="Age">
        <input
          name="age"
          placeholder="Age"
          type="number"
          value={data.age}
          onChange={handleChange}
          className={error.age ? 'error' : ''}
        />
      </FormControl>

      <FormControl label="Employed">
      <input
          type="checkbox"
          name="employed"
          checked={data.employed}
          onChange={handleChange}
        />
      </FormControl>

      
      <FormControl label="Favourite Color">
      <select
          name="selectedColor"
          value={data.selectedColor}
          onChange={handleChange}
        >
          <option></option>
          <option value="blue">Blue</option>
          <option value="green">Green</option>
          <option value="red">Red</option>
          <option value="purple">Purple</option>
        </select>
      </FormControl>

      <FormControl label="Sauces">
        {
            sauces.map(item=>(
                <Fragment key={item.value}>
                   <input type="checkbox" name="sauces"
                   value={item.value} onChange={handleChange}
                   checked={data.sauces.includes(item.value)}/>
                   <span>{item.label}</span>
                </Fragment>
            ))
        }

      </FormControl>

      <FormControl label="Best Stooge">
         {
            stooges.map(item=>(
                <Fragment key={item.value} >   
                <input type="radio" name="stooge"
                 value={item.value} onChange={handleChange}
                checked={item.value === data.stooge} />
                <span>{item.label}</span> 
                </Fragment>
            ))
         }
      </FormControl>

      <FormControl label="Note">
        <textarea
          name="note"
          placeholder="Note"
          value={data.note}
          onChange={handleChange}
          className={error.note ? 'error' : ''}
        />
      </FormControl>


      <button type="submit" disabled={!isFormValid}>Submit</button>
      <button type="reset" disabled={!isFormChanged} onClick={handleReset}>Reset</button>

  
    </form>

    <pre>
       {displayData}
    </pre>
    </>
  );
}
