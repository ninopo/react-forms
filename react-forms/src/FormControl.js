export default function FormControl({label, children}){
    return (
        <div>
            <label>
               {label}
            </label>
            {children}
        </div>
    )
}