import { useState } from "react";
import "./form.css";

export default function MyForm() {

    const initialFormData = {
        firstName: "",
        lastName: "",
        age: "",
        employed: false,
        selectedColor: "",
        sauces: [],
        stooge: "larry",
        note: "",
      };

      const [formData, setFormData] = useState(initialFormData);

      const [fieldValidity, setFieldValidity] = useState({
        firstName: true,
        lastName: true,
        age: true,
        note: true,
      });
      const [touchedFields, setTouchedFields] = useState({});
    
      const {
        firstName,
        lastName,
        age,
        employed,
        selectedColor,
        sauces,
        stooge,
        note,
      } = formData;

      const isAlphabetic = (value) => /^[A-Za-z\s]+$/.test(value);
      const isNumeric = (value) => /^[0-9]+$/.test(value);
    
      const validateField = (fieldName) => {
        switch (fieldName) {
          case "firstName":
          case "lastName":
            return isAlphabetic(formData[fieldName]);
          case "age":
            return isNumeric(formData[fieldName]);
          case "note":
            return formData[fieldName].length <= 100;
          default:
            return true;
        }
      };

      const handleChange = (e) => {
        const { name, value, type, checked } = e.target;
        const isValid = validateField(name);
       
        const newValue = type === "checkbox" ? checked : value;

        setFieldValidity({
            ...fieldValidity,
            [name]: isValid,
          });
    
        if (type === "radio") {
            setFormData({
              ...formData,
              [name]: value,
            });
          } else {
            setFormData({
              ...formData,
              [name]: newValue,
            });
          }

          if (name === "sauces" && type === "checkbox") {
            if (checked) {
              setFormData({
                ...formData,
                sauces: [...sauces, value],
              });
            } else {
              setFormData({
                ...formData,
                sauces: sauces.filter((sauce) => sauce !== value),
              });
            }
          } else {
            setFormData({
              ...formData,
              [name]: newValue,
            });
          }

          setTouchedFields({
            ...touchedFields,
            [name]: true,
          });
      };

      const handleReset = () => {
        setFormData(initialFormData);

        setFieldValidity({
            firstName: true,
            lastName: true,
            age: true,
            note: true,
          });
          setTouchedFields({});
      };

      const handleSubmit = (e) => {
        e.preventDefault();

        const isValid = Object.keys(formData).every((fieldName) =>
        validateField(fieldName)
      );
  
      if (!isValid) {
        return;
      }

    

      alert(JSON.stringify(formData, null, 2));
    
        console.log("Form Data:", formData);
    
      };
  
    
      const isFieldInvalid = (fieldName) =>
    !fieldValidity[fieldName] && touchedFields[fieldName];
  



  return (
    <form onSubmit={handleSubmit}>
      <label>
        First Name{" "}
        <input
          name="firstName"
          placeholder="First Name"
          value={firstName}
          onChange={handleChange}
          className={isFieldInvalid("firstName") ? "invalid" : ""}
        />
      </label>

      <label>
        Last Name{" "}
        <input
          name="lastName"
          placeholder="Last Name"
          value={lastName}
          onChange={handleChange}
          className={isFieldInvalid("lastName") ? "invalid" : ""}
        />
      </label>

      <label>
        Age{" "}
        <input
          name="age"
          placeholder="Age"
          type="number"
          value={age}
          onChange={handleChange}
          className={isFieldInvalid("age") ? "invalid" : ""}
        />
      </label>

      <label>
        Employed{" "}
        <input
          type="checkbox"
          name="employed"
          checked={employed}
          onChange={handleChange}
        />
      </label>

      <label>
        Favourite Color{" "}
        <select
          name="selectedColor"
          value={selectedColor}
          onChange={handleChange}
        >
          <option></option>
          <option value="blue">Blue</option>
          <option value="green">Green</option>
          <option value="red">Red</option>
          <option value="purple">Purple</option>
        </select>
      </label>

      <p>
        Sauces{" "}
        <label>
          <input type="checkbox" name="sauces" value="ketchup" checked={sauces.includes("ketchup")} onChange={handleChange} /> Ketchup
        </label>
        <label>
          <input type="checkbox" name="sauces" value="mustard" checked={sauces.includes("mustard")} onChange={handleChange} /> Mustard
        </label>
        <label>
          <input type="checkbox" name="sauces" value="mayonnaise" checked={sauces.includes("mayonnaise")} onChange={handleChange} /> Mayonnaise
        </label>
        <label>
          <input type="checkbox" name="sauces" value="guacamole" checked={sauces.includes("guacamole")} onChange={handleChange} /> Guacamole
        </label>
      </p>

      <p>
        Best Stooge{" "}
        <label>
          <input type="radio" name="stooge" value="larry" checked = {stooge === "larry"} onChange={handleChange} /> Larry
        </label>
        <label>
          <input type="radio" name="stooge" value="moe" checked={stooge === "moe"} onChange={handleChange} /> Moe
        </label>
        <label>
          <input type="radio" name="stooge" value="curly" checked={stooge === "curly"} onChange={handleChange} /> Curly
        </label>
      </p>

      <label>
        Notes{" "}
        <textarea
          placeholder="Notes"
          name="note"
          rows={4}
          cols={40}
          value={note}
          onChange={handleChange}
        />
      </label>

      <button type="submit">Submit</button>
      <button type="reset" onClick={handleReset}>Reset</button>
    </form>
  );
}
